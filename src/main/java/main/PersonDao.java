package main;

import model.Person;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PersonDao {

    private JdbcTemplate template;

    public PersonDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Person> getAllPersons() {
        String sql = "select id, name from person";

        throw new RuntimeException("not implemented yet");
    }

    public Person insertPerson(Person person) {
        throw new RuntimeException("not implemented yet");
    }

    private static class PersonMapper implements RowMapper<Person> {
        @Override
        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {

            Person person = new Person();

            return person;
        }
    }
}
